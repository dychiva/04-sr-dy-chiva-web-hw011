import React, {Component} from 'react'
import style from './style.css';
import HistoryResult from './HistoryResult'
import 'bootstrap/dist/css/bootstrap.css';
import {
    Button,
    Card,
    InputGroup,
    FormControl,
    Form,
    ListGroup
} from 'react-bootstrap'

class Calculator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number1: '',
            number2: '',
            op: '+',
            result: '',
            arr: [],
            test: 'chiva'
        };
    }
    onChangeNumber1 = (event) => {
        this.setState({number1: event.target.value});
    }
    onChangeNumber2 = (event) => {
        this.setState({number2: event.target.value});
    }
    handleChange = (event) => {
        this.setState({op: event.target.value});
    }

    handleCal = () => {
        var re;
        if (this.state.op === '+') {
            re = Number(this.state.number1) + Number(this.state.number2);
        } else if (this.state.op === '-') {
            re = Number(this.state.number1) - Number(this.state.number2);
        } else if (this.state.op === '*') {
            re = Number(this.state.number1) * Number(this.state.number2);
        } else if (this.state.op === '/') {
            re = Number(this.state.number1) / Number(this.state.number2);
        } else {
            re = Number(this.state.number1) % Number(this.state.number2);
        }

        if (isNaN(this.state.number1) || isNaN(this.state.number2)) {
            alert('It is not a Number');
        } else {
            var arr = this.state.arr;
            arr.push(re);
            this.setState({result: re, arr: arr})
            // this.onGreet(re);
        }
    }
    onGreet = (re) => {
        var arr = this.state.arr;
        arr.push(re);
        this.setState({result: re, arr: arr})
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-6">
                        <from>
                            <Card style={
                                {width: '18rem'}
                            }>
                                <Card.Img variant="top"
                                    src={
                                        require('./image/calculatorImage.png')
                                    }/>
                                <Card.Body>

                                    <InputGroup className="mb-3">
                                        <FormControl type="text" pattern="[0-9]*"
                                            onChange={
                                                this.onChangeNumber1
                                            }
                                            placeholder="Enter Num1"
                                            aria-label="Username"
                                            aria-describedby="basic-addon1"/>
                                    </InputGroup>

                                    <InputGroup className="mb-3">
                                        <FormControl type="text" pattern="[0-9]*"
                                            onChange={
                                                this.onChangeNumber2
                                            }
                                            placeholder="Enter Num2"
                                            aria-label="Username"
                                            aria-describedby="basic-addon1"/>
                                    </InputGroup>

                                    <Form.Group controlId="exampleForm.SelectCustomSizeSm">
                                        <Form.Label>Select your operator</Form.Label>
                                        <Form.Control as="select"
                                            onChange={
                                                this.handleChange
                                            }
                                            value={
                                                this.state.op
                                            }
                                            size="md"
                                            custom>
                                            <option value="+">+ Plus</option>
                                            <option value="-">- Substract</option>
                                            <option value="*">* Multiply</option>
                                            <option value="/">/ Devide</option>
                                            <option value="%">% Module</option>
                                        </Form.Control>
                                    </Form.Group>

                                    <Button variant="primary"
                                        onClick={
                                            this.handleCal
                                    }>Calculate</Button>

                                </Card.Body>
                            </Card>

                        </from>

                    </div>
                    <div className="col-sm-6">
                        <h2>Result History</h2>
                        <br/> {/* <label> {
                            this.state.arr.map((word, idx) => <li key={idx}>
                                {word}</li>)
                        } </label> */}


                        <Card style={
                            {width: '18rem'}
                        }>
                            <ListGroup variant="flush">
                                {
                                this.state.arr.map((word, idx) => <ListGroup.Item key={idx}>
                                    {word}</ListGroup.Item>)
                            } </ListGroup>
                        </Card>
                    </div>
                </div>

                {/* <HistoryResult greet={this.handleCal}/> */} </div>
        );
    }
}
export default Calculator;
